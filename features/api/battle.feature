Feature: Battle Resource
  In order to prove my programmers' worth against projects
  As an API client
  I need to be able to start and view battles

  Background:
    Given the user "weaverryan" exists
    And "weaverryan" has an authentication token "ABCD123"
    And I set the "Authorization" header to be "token ABCD123"

  Scenario: Create a new Battle
    Given there is a programmer called "Yoda"
    And there is a project called "jedy_project"
    Given I have the payload:
      """
        {
          "programmerId": "%programmers.Yoda.id%",
          "projectId": "%projects.jedy_project.id%"
        }
      """
    When I request "POST /api/battles"
    Then the response status code should be 201
    And the "Location" header should exist
    And the "didProgrammerWin" property should exist

  Scenario: GET'ing a single battle
    Given there is a programmer called "Yoda"
    And there is a project called "jedy_project"
    And there has been a battle between "Yoda" and "jedy_project"
    When I request "GET /api/battles/%battles.last.id%"
    Then the response status code should be 200
    And the following properties should exist:
    """
    didProgrammerWin
    notes
    """
    And the link "programmer" should exist and its value should be "/api/programmers/Yoda"
    And the "Content-Type" header should be "application/hal+json"
    And the embedded "programmer" should have a "nickname" property equal to "Yoda"

Feature: Token
  In order to access restricted information
  As an IPA client
  I can create tokens and use them to access information

  Scenario: Create a token
    Given there is a user "weaverryan" with password "test"
    And I have the payload:
      """
      {
        "notes": "A testing token!"
      }
      """
    And I authenticate with user "weaverryan" and password "test"
    When I request "POST /api/tokens"
    Then the response status code should be 201
#    And the "Location" header should exist
    And the "token" property should be a string

  Scenario: Create a token with wrong password
    Given there is a user "weaverryan" with password "test"
    And I have the payload:
      """
      {
        "notes": "A testing token!"
      }
      """
    And I authenticate with user "weaverryan" and password "BEMBA"
    When I request "POST /api/tokens"
    Then the response status code should be 401

  Scenario: Create a token without notes
    Given there is a user "weaverryan" with password "test"
    And I authenticate with user "weaverryan" and password "test"
    When I request "POST /api/tokens"
    Then the response status code should be 400
#    And the "Location" header should exist
    And the "errors.notes" property should equal "Please add some notes about this token"

<?php

require __DIR__.'/vendor/autoload.php';

use Guzzle\Http\Client;

// create our http client (Guzzle)
$client = new Client('http://localhost:8000', array(
    'request.options' => array(
        'exceptions' => false,
    )
));

$nickname = 'ObjectOriented' . rand(0, 999);
$obj = [
    'nickname' => $nickname,
    'avatarNumber' => 6,
    'tagLine' => 'A test dev!',
];
$request = $client->post('/api/programmers', null, json_encode($obj));
$response = $request->send();

$url = $response->getHeader('Location');
$request = $client->get($url);
$response = $request->send();

$request = $client->get('/api/programmers');
$response = $request->send();

$request = $client->patch('/api/programmers/' . $nickname, null, json_encode(['tagLine' => 1]));
$response = $request->send();
echo $response;
echo "\n\n";


<?php

namespace KnpU\CodeBattle\Controller\Api;

use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\PaginatedRepresentation;
use KnpU\CodeBattle\Api\ApiProblem;
use KnpU\CodeBattle\Api\ApiProblemException;
use KnpU\CodeBattle\Controller\BaseController;
use KnpU\CodeBattle\Model\Homepage;
use KnpU\CodeBattle\Model\Programmer;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProgrammerController extends BaseController
{
    protected function addRoutes(ControllerCollection $controllers)
    {
        $controllers->get('/api', [$this, 'homepageAction'])
                    ->bind('api_homepage');
        $controllers->post('/api/programmers', [$this, 'newAction']);
        $controllers->get('/api/programmers', [$this, 'listAction'])
                    ->bind('api_programmers_list');
        $controllers->put('/api/programmers/{nickname}', [$this, 'updateAction']);
        $controllers->patch('/api/programmers/{nickname}', [$this, 'updateAction']);
        $controllers->delete('/api/programmers/{nickname}', [$this, 'deleteAction']);
        $controllers->get('/api/programmers/{nickname}', [$this, 'showAction'])
                    ->bind('api_programmer_show');
        $controllers->get('/api/programmers/{nickname}/battles', [$this, 'showBattlesListAction'])
                    ->bind('api_programmer_battles_list');
    }

    public function homepageAction()
    {
        $homepage = new Homepage();

        return $this->createApiResponse($homepage);
    }

    public function newAction(Request $request)
    {
        $this->enforceUserSecurity();

        $programmer = new Programmer();

        $this->handleRequest($request, $programmer);

        $errors = $this->validate($programmer);

        if (!empty($errors)) {
            $this->throwApiProblemValidationException($errors);
        }

        $this->save($programmer);

        $url = $this->generateUrl('api_programmer_show', [
            'nickname' => $programmer->nickname,
        ]);

        $response = $this->createApiResponse($programmer, 201);
        $response->headers->set('Location', $url);

        return $response;
    }

    public function updateAction(Request $request, $nickname)
    {
        $programmer = $this->getProgrammerRepository()->findOneByNickname($nickname);

        if (!$programmer) {
            $this->throw404('Oh no!');
        }

        $this->enforceProgrammerOwnershipSecurity($programmer);

        $this->handleRequest($request, $programmer);

        $errors = $this->validate($programmer);

        if (!empty($errors)) {
            $this->throwApiProblemValidationException($errors);
        }

        $this->save($programmer);

        return $this->createApiResponse($programmer);
    }

    public function deleteAction($nickname)
    {
        $programmer = $this->getProgrammerRepository()
                           ->findOneByNickname($nickname);

        $this->enforceProgrammerOwnershipSecurity($programmer);

        if ($programmer) {
            $this->delete($programmer);
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    public function showAction($nickname)
    {
        $programmer = $this->getProgrammerRepository()->findOneByNickname($nickname);

        if (!$programmer) {
            $this->throw404('Oh no! There are no programmers!');
        }

        $response = $this->createApiResponse($programmer);

        return $response;
    }

    public function showBattlesListAction($nickname)
    {
        $programmer = $this->getProgrammerRepository()->findOneByNickname($nickname);

        if (!$programmer) {
            $this->throw404('Oh no! There are no programmers!');
        }

        $battles = $this->getBattleRepository()->findAllBy([
            'programmerId' => $programmer->id,
        ]);

        $collection = new CollectionRepresentation($battles, 'battles', 'battles');

        $response = $this->createApiResponse($collection);

        return $response;
    }

    public function listAction(Request $request)
    {
        $filter_nickname = $request->query->get('nickname');

        if ($filter_nickname) {
            $programmers = $this->getProgrammerRepository()->findAllLike([
                'nickname' => '%' . $filter_nickname . '%',
            ]);
        }
        else {
            $programmers = $this->getProgrammerRepository()->findAll();
        }

        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 5);
        $number_of_pages = ceil(count($programmers) / 5);
        $offset = ($page - 1) * $limit;

        $collection = new CollectionRepresentation(
            array_slice($programmers, $offset, $limit),
            'programmers',
            'programmers');

        $paginated = new PaginatedRepresentation(
            $collection,
            'api_programmers_list',
            [],
            $page,
            $limit,
            $number_of_pages
        );

        $response = $this->createApiResponse($paginated);

        return $response;
    }

    private function handleRequest(Request $request, Programmer $programmer)
    {
        $data = $this->decodeRequestBodyIntoParameters($request);

        $properties = ['avatarNumber', 'tagLine'];

        $is_new = !$programmer->id;

        if ($is_new) {
            $properties[] = 'nickname';
        }

        foreach ($properties as $property) {
            if ($request->isMethod('PATCH') && !$data->has($property)) {
                continue;
            }

            $programmer->$property = $data->get($property);
        }

        $programmer->userId = $this->getLoggedInUser()->id;
    }

}

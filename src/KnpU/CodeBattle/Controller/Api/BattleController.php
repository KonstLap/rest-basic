<?php

namespace KnpU\CodeBattle\Controller\Api;

use KnpU\CodeBattle\Controller\BaseController;
use KnpU\CodeBattle\Model\Programmer;
use KnpU\CodeBattle\Model\Project;
use KnpU\CodeBattle\Security\Token\ApiToken;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

class BattleController extends BaseController
{

    protected function addRoutes(ControllerCollection $controllers)
    {
        $controllers->post('/api/battles', [$this, 'newAction']);
        $controllers->get('/api/battles/{id}', [$this, 'showAction'])
                    ->bind('api_battles_show');
    }

    public function newAction(Request $request)
    {
        $this->enforceUserSecurity();

        $data = $this->decodeRequestBodyIntoParameters($request);

        $project_id = $data->get('projectId');
        $programmer_id = $data->get('programmerId');

        $errors = [];

        /** @var Programmer $programmer */
        $programmer = $this->getProgrammerRepository()->find($programmer_id);
        /** @var Project $project */
        $project = $this->getProjectRepository()->find($project_id);

        if (!$project) {
            $errors['projectId'] = "Missing a value of projectId parameter";
        }
        if (!$programmer) {
            $errors['programmerId'] = "Missing a value of programmerId parameter";
        }
        if ($errors) {
            $this->throwApiProblemValidationException($errors);
        }

        $battle = $this->getBattleManager()->battle($programmer, $project);

        $url = $this->generateUrl('api_battles_show', ['id' => $battle->id]);

        $response = $this->createApiResponse($battle, 201);
        $response->headers->set('Location', $url);

        return $response;
    }

    public function showAction($id)
    {
        $battle = $this->getBattleRepository()->find($id);

        if (!$battle) {
            $this->throw404('No battles was found for the id ' . $id);
        }

        return $this->createApiResponse($battle);
    }

}

<?php

namespace App\KnpU\CodeBattle\Tests;


use Guzzle\Http\Client;

class ProgrammerControllerTest extends \PHPUnit_Framework_TestCase
{

    public function testPost()
    {
        $client = new Client('http://localhost:8000', array(
            'request.options' => array(
                'exceptions' => false,
            )
        ));

        $nickname = 'ObjectOriented' . rand(0, 999);
        $obj = [
            'nickname' => $nickname,
            'avatarNumber' => 6,
            'tagLine' => 'A test dev!',
        ];
        $request = $client->post('/api/programmers', null, json_encode($obj));
        $response = $request->send();

        $data = json_decode($response->getBody(true), true);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->hasHeader('Location'));
        $this->assertArrayHasKey('nickname', $data);
    }

}

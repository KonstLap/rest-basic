<?php

namespace KnpU\CodeBattle\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "api_homepage"
 *      ),
 *     attributes={"title": "The API Homepage"}
 * )
 * @Hateoas\Relation(
 *      "programmers",
 *      href = @Hateoas\Route(
 *          "api_programmers_list"
 *      ),
 *      attributes={"title": "The programmers list"}
 * )
 */
class Homepage
{

    private $message = 'Welcome to the CodeBattle API';

}

<?php

namespace KnpU\CodeBattle\Api;

use Symfony\Component\HttpFoundation\Response;

class ApiProblem
{
    const TYPE_VALIDATION_ERROR = 'validation_error';
    const TYPE_INVALID_REQUEST_BODY_FORMAT_ERROR = 'invalid_body_format';
    const TYPE_AUTHENTICATION_ERROR = 'authentication_error';

    private static $titles = [
        self::TYPE_VALIDATION_ERROR => 'There was a validation error',
        self::TYPE_INVALID_REQUEST_BODY_FORMAT_ERROR => 'Invalid JSON format sent',
        self::TYPE_AUTHENTICATION_ERROR => 'Invalid or missing authentication',
    ];

    private $status_code;

    private $type;

    private $title;

    private $extra_data = [];

    public function __construct($status_code, $type = null)
    {
        $this->status_code = $status_code;
        $this->type = $type;

        if ($type === null) {
            $this->type = 'about:blank';
            $this->title = isset(Response::$statusTexts[$status_code])
                ? Response::$statusTexts[$status_code]
                : 'Unknown status code :(';
        }
        else {
            if (!isset(self::$titles[$type])) {
                throw new \Exception(sprintf(
                    'No title for type "%t". Did you make it up?',
                    $type
                ));
            }
            $this->title = self::$titles[$type];
        }

    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getExtraData(): array
    {
        return $this->extra_data;
    }

    public function set($name, $value)
    {
        $this->extra_data[$name] = $value;
    }

    public function toArray()
    {
        return array_merge(
            $this->getExtraData(),
            [
                'status' => $this->getStatusCode(),
                'type' => $this->getType(),
                'title' => $this->getTitle(),
            ]
        );
    }

}

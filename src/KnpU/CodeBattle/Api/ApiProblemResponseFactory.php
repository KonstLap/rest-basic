<?php

namespace KnpU\CodeBattle\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiProblemResponseFactory
{

    public function createResponse(ApiProblem $api_problem)
    {
        $data = $api_problem->toArray();
        if ($data['type'] !== 'about:blank') {
            $data['type'] = 'http://localhost:8000/docs/errors#' . $data['type'];
        }

        $response = new JsonResponse(
            $data,
            $api_problem->getStatusCode()
        );
        $response->headers->set('Content-Type', 'application/problem+json');

        return $response;
    }

}

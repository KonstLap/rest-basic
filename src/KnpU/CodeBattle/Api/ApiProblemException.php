<?php

namespace KnpU\CodeBattle\Api;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiProblemException extends HttpException
{

    /**
     * @var ApiProblem;
     */
    private $api_problem;

    public function __construct(ApiProblem $api_problem ,\Exception $previous = NULL, array $headers = [], $code = 0)
    {
        $this->api_problem = $api_problem;
        $statusCode = $api_problem->getStatusCode();
        $message = $api_problem->getTitle();

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    /**
     * @return ApiProblem
     */
    public function getApiProblem(): ApiProblem
    {
        return $this->api_problem;
    }



}
